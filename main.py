## TODO:
# - Have a list of common ingredients densities so if a recipe gives it in volume, weight can be calculated.
# - Have option to scale recipe.
# - Add instructions.
# - Only can press next when scales is stable.
# - Show list of ingrediands needed, and remove when not needed any more.
# - Add navigation to see next steps.
# - Add app connected by bluetooth that shows the progress of each step.
# - Add timer.

import machine
import ssd1306
from hx711 import HX711
from utime import sleep_us
from math import floor
from machine import Pin
import time

class Scales(HX711):
    def __init__(self, d_out, pd_sck):
        super(Scales, self).__init__(d_out, pd_sck)
        self.offset = 0

    def reset(self):
        self.power_off()
        self.power_on()

    def tare(self):
        self.offset = self.read()

    def raw_value(self):
        return self.read() - self.offset

    def grams(self):
        return floor(self.raw_value()*955/414800)

scales = Scales(d_out=23, pd_sck=19)
scales.tare()

WEIGHT_TO_G = {
  "gram": 1,
  "kilogram": 1000,
  "ounce": 28.35
}

VOLUMES_TO_ML = {
  "tbsp": 14.8,
  "tsp": 4.9,
  "cup": 237,
  "pint": 473
}

DENSITIES = {
  "flour": 0.5,
  "water": 1,
  "sugar": 0.85,
  "salt": 1.2,
  "oil": 0.913
}

class Step():
    def __init__(self, text, ingredient=None, amount=None, unit="g"):
        self.text = text
        self.ingredient = ingredient
        self.grams = None
        self.amount = amount
        self.unit = unit

        if amount:
            if unit in VOLUMES_TO_ML:
                ml = amount * VOLUMES_TO_ML[unit]
                if ingredient in DENSITIES:
                    self.grams = ml * DENSITIES[ingredient]
            elif unit in WEIGHT_TO_G:
                self.grams = amount * WEIGHT_TO_G[unit]
            else:
                print(f"Unknown unit: {unit}")
        
        if self.grams:
            self.grams = round(self.grams)

class Recipe():
    def __init__(self):
        self.steps = []
    
    def add_step(self, step):
        self.steps.append(step)

    def read_recipe_from_file(self, file):
        # TODO
        return 

cake = Recipe()
cake.add_step(Step("Preheat oven to 200C."))
cake.add_step(Step("Place a bowl for dry ingredients on scales with sieve on top."))
cake.add_step(Step("Add flour.", "flour", 250))
cake.add_step(Step("Add baking powder.", "baking powder", 17))
cake.add_step(Step("Add salt.", "salt", "1/2 tsp"))
cake.add_step(Step("Add sugar.", "sugar", 13))
cake.add_step(Step("Mix ingredients together in sieve then sieve into bowl."))
cake.add_step(Step("Move bowl off scales and add bowl to measure dates."))
cake.add_step(Step("Add pitted dates.", "pitted dates", 200))
cake.add_step(Step("Chop dates into thirds then mix into dry ingredients."))
cake.add_step(Step("Add bowl to mix oil and milk."))
cake.add_step(Step("Add milk", "milk", 184))
cake.add_step(Step("Add olive oil.", "olive oil", 53))
cake.add_step(Step("Make a hole in the middle of the dry ingredients for adding wet ingredients."))
cake.add_step(Step("Mix wet ingredients together then pour into dry ingredients. Mix just until ingredients are combined."))
cake.add_step(Step("Place dough on a floured baking tray and gently pat down until it is about 1.5 cm thick. Cut into scones and bake in oven for around 15 minutes."))
#cake.add_step(Step("Start 15 minute timer", timer=15*60))

bread = Recipe()
bread.add_step(Step("Add hot water", "water", 1, "cup"))
bread.add_step(Step("Add flour", "flour", 3, "cup"))
bread.add_step(Step("Add oil", "oil", 0.25, "cup"))
bread.add_step(Step("Add sugar", "sugar", 2, "tbsp"))
bread.add_step(Step("Add salt", "salt", 1, "tsp"))
bread.add_step(Step("Add yeast", "yeast", 0.25, "ounce"))

# OLED configuration
i2c = machine.I2C(scl=machine.Pin(22), sda=machine.Pin(21))
oled = ssd1306.SSD1306_I2C(128, 64, i2c)

# Mock method for reading scale weight. Replace this with actual ADC reading or your scale interface.
def read_weight():
    return scales.grams()

def draw_progress_bar(percentage):
    width = 100
    height = 8
    x = (128-width)//2
    y = 64-height
    oled.fill_rect(0, y, 128, height, 0) # Clear the progress bar
    oled.rect(x, y, width, height, 1) # Draw the outline of the progress bar
    oled.fill_rect(x, y, int(width * percentage), height, 1) # Draw the filled portion

button = Pin(27, Pin.IN, Pin.PULL_UP)
def button_pressed():
    return button.value() == 0

def split_string_for_oled(s):
    words = s.split()
    lines = []
    current_line = ""
    
    for word in words:
        if len(current_line) + len(word) <= 15:
            if current_line:
                current_line += " " + word
            else:
                current_line = word
        else:
            lines.append(current_line)
            current_line = word
            
    if current_line:
        lines.append(current_line)
    return lines


def print_text(text, tick):
    lines = split_string_for_oled(text)
    if len(lines) < 6:
        print_lines(lines, 0)
        return
    
    a = (len(lines)+1)*10
    offset1 = -1*(tick%a)
    offset2 = offset1 + a
    print_lines(lines, offset1)
    print_lines(lines, offset2)
    
    
def print_lines(lines, offset):
    y = offset
    for line in lines:
        oled.text(line, 0, y)
        y += 10

mult = 2/3

for step in bread.steps:
    # TODO Have better de-bouncing code
    time.sleep(0.1)
    scales.tare()
    while button_pressed():
        time.sleep(0.1)
    time.sleep(0.1)
    oled.fill(0)

    if step.ingredient == None:
        i = 0
        while True:
            oled.fill(0)
            print_text(step.text, i)
            oled.show()
            if button_pressed():
                break
            time.sleep(0.01)
            i += 1
        continue
    
    # Don't have it converted to weight
    if step.grams == None:
        oled.text(step.text, 0, 0)
        oled.text("{} {}".format(step.amount*mult, step.unit), 0, 10)
        oled.show()
        while True:
            if button_pressed():
                break
        continue

    oled.text(step.text, 0, 0)
    
    oled.show()
    while True:
        current_weight = read_weight()
        ingredient_done = current_weight >= step.grams * mult

        oled.fill_rect(0, 10, 64, 10, 0) # Clear old text
        s = str(current_weight) +"/"+str(step.grams * mult)+"g"
        if ingredient_done:
            s += " done."
        oled.text(s, 0, 10)
        draw_progress_bar(current_weight / (step.grams * mult))
        oled.show()

        if button_pressed():
            if ingredient_done:
                break # Go to next ingredient
            else:
                # TODO Check if user wants to go to next ingredient
                break

